//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
//Libreria para trabajo con body (peticiones post)
var bodyparser = require('body-parser');
app.use(bodyparser.json())

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
var movimientosJSON = require('./movimientosv2.json');

//Se codifica get
app.get('/',function(req,res) {
  //res.send('Hola we nodeJS');
  //console.log('GET');
  res.sendFile(path.join(__dirname, 'index.html'));
});

//Se define  url Clientes
app.get('/Clientes',function(req,res) {
  res.send("Aqui estan los clientes nuevos");
});

//Se define url Clientes por id
app.get('/Clientes/:idcliente',function(req,res) {
  res.send("cliente numero:"+ req.params.idcliente);
});

//Se define url para regresar Movimientos Json v1
app.get('/Movimientos/v1',function(req,res) {
  res.sendfile("movimientosv1.json");
});

//Se define url para regresar Movimientos Json v2
app.get('/Movimientos/v2',function(req,res) {
  //res.sendfile("movimientosv2.json");
  res.send(movimientosJSON);
});

//Se define url para regresar Movimientos Json v2 por index
//Se puede enviar otro parametro seguido del primero :otro e imprimir
app.get('/Movimientos/v2/:index',function(req,res) {
  console.log(req.params.index-1);
  res.send(movimientosJSON[req.params.index-1]);
});

//Se define url para regresar query
app.get('/MovimientosQ/v2',function(req,res) {
  console.log(req.query);
  res.send('Recibido');
});



//Se codifica post
app.post('/',function(req,res) {
  res.send('Hemos recibido peticion POST');
  console.log('POST');
});

//Se codifica post
app.post('/Movimientos/v2',function(req,res) {
  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  res.send('Hemos dado de ALTA');
});
